﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace Insurance.Areas.Admin.Controllers
{
    public class PolicyInsuranceController : Controller
    {
        // GET: Admin/PolicyInsurance
        public ActionResult Index()
        {
            var dao = new PolicyInsuranceDao();
            var model = dao.Index();
            return View(model);
        }

        // GET: Admin/PolicyInsurance/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Admin/PolicyInsurance/Create
        public ActionResult Create(int? id)
        {
            Model1 db = new Model1();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            PolicyInsurance policyInsurance= db.PolicyInsurances.Find(id);
            if (policyInsurance == null)
                return HttpNotFound();
            ViewBag.MessageError = "Lỗi";
            return View();
        }

        // POST: Admin/PolicyInsurance/Create
        [HttpPost]
        public ActionResult Create(PolicyInsurance newPolicyInsurance,int id)
        {
            if (ModelState.IsValid)
            {
                
                var dao = new PolicyInsuranceDao();
                bool policyInsurance = dao.Insert(newPolicyInsurance,id);
                if (policyInsurance == true)
                {
                    return RedirectToAction("Index", "PolicyInsurance");
                }
               
            }

            ViewBag.MessageError = "Nhập lại mã bảo hiểm của bạn.";
            ViewBag.idInsurance = id;
            return View();
        }

        // GET: Admin/PolicyInsurance/Edit/5
        public ActionResult Edit(int id)
        {
            var policyInsurance = new PolicyInsuranceDao().find(id);
            return View(policyInsurance);
        }

        // POST: Admin/PolicyInsurance/Edit/5
        [HttpPost]
        public ActionResult Edit(PolicyInsurance policyInsurance)
        {
            if (ModelState.IsValid)
            {
                var dao = new PolicyInsuranceDao();
                var result = dao.Update(policyInsurance);
                if (result == true)
                {
                    return RedirectToAction("Index", "PolicyInsurance");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(policyInsurance);
        }

        // POST: Admin/PolicyInsurance/Delete/5
        [HttpDelete]
        public ActionResult Delete(int? id)
        {
            try
            {
                var policyInsurance = new PolicyInsuranceDao().Delete(id);
                return RedirectToAction("Index","PolicyInsurance");
            }
            catch
            {
                return View();
            }
        }
    }
}
