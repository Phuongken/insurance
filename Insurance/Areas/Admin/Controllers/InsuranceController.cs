﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Model.Dao;
using Model.EF;

namespace Insurance.Areas.Admin.Controllers
{
    public class InsuranceController : Controller
    {
        // GET: Admin/Insurance
        public ActionResult Index()
        {
            var dao = new InsuranceDao();
            var model = dao.Index();
            return View(model);
        }

        // GET: Admin/Insurance/Details/5
        public ActionResult Details(int? id)
        {
            Model1 db = new Model1();
            var dao = new Insurance1();
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Insurance1 insurance1 = db.Insurance1.Find(id);
            if (insurance1 == null)
                return HttpNotFound();
            return View(insurance1);
        }
        [HttpGet]
        // GET: Admin/Insurance/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Admin/Insurance/Create
        [HttpPost]
        public ActionResult Create(Insurance1 newInsurance)
        {
            if (ModelState.IsValid)
            {
                var dao = new InsuranceDao();
                
                    bool insurance = dao.Insert(newInsurance);
                if (insurance != null)
                {
                    return RedirectToAction("Index", "Insurance");
                }
            }
            return View(newInsurance);
        }

        // GET: Admin/Insurance/Edit/5
        public ActionResult Edit(int id)
        {
            var insurance = new InsuranceDao().find(id);
            return View(insurance);
        }

        // POST: Admin/Insurance/Edit/5
        [HttpPost]
        public ActionResult Edit(Insurance1 insurance1)
        {
            if(ModelState.IsValid)
            {
                var dao = new InsuranceDao();
                var result = dao.Update(insurance1);
                if (result == true)
                {
                    return RedirectToAction("Index", "Insurance");
                }
                else
                {
                    ModelState.AddModelError("", "Chỉnh sửa thông tin thất bại");
                }
            }
            return View(insurance1);
        }

        // POST: Admin/Insurance/Delete/5
        [HttpDelete]
        public ActionResult Delete(int? id)
        {
            try
            {
                var post = new InsuranceDao().Delete(id);
                return RedirectToAction("Index", "Insurance");
            }
            catch
            {
                return View();
            }
        }
    }
}
