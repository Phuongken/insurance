namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class PeopleSayU
    {
        public int ID { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Required]
        [StringLength(250)]
        public string Avatar { get; set; }

        [Required]
        [StringLength(250)]
        public string Address { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Content { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateAt { get; set; }

        public DateTime ModifiledDate { get; set; }

        public DateTime ModifiledAt { get; set; }

        public int Status { get; set; }

        [Required]
        [StringLength(150)]
        public string Job { get; set; }
    }
}
