namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("OrderDetail")]
    public partial class OrderDetail
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int InsurantID { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ContractID { get; set; }

        public int Quantity { get; set; }

        public decimal Price { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        [Required]
        [StringLength(250)]
        public string FullName { get; set; }

        public int HomePhone { get; set; }

        public int MobilePhone { get; set; }

        [Required]
        [StringLength(50)]
        public string Email { get; set; }

        [Required]
        [StringLength(250)]
        public string AddressDetail { get; set; }

        public int AddressID { get; set; }

        [Required]
        [StringLength(10)]
        public string AddressContract { get; set; }

        public int AddressIDContract { get; set; }

        [Required]
        [StringLength(250)]
        public string FullNameReceive { get; set; }

        public DateTime Dob { get; set; }

        public int Identity { get; set; }

        public int Relationship { get; set; }

        public int Gender { get; set; }

        [Required]
        [StringLength(250)]
        public string Job { get; set; }
    }
}
