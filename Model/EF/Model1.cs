namespace Model.EF
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class Model1 : DbContext
    {
        public Model1()
            : base("name=Model1")
        {
        }

        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<Admin> Admins { get; set; }
        public virtual DbSet<Contact> Contacts { get; set; }
        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<District> Districts { get; set; }
        public virtual DbSet<InfoOnline> InfoOnlines { get; set; }
        public virtual DbSet<Insurance1> Insurance1 { get; set; }
        public virtual DbSet<OrderDetail> OrderDetails { get; set; }
        public virtual DbSet<PayMent> PayMents { get; set; }
        public virtual DbSet<PeopleSayU> PeopleSayUs { get; set; }
        public virtual DbSet<PolicyInsurance> PolicyInsurances { get; set; }
        public virtual DbSet<Provind> Provinds { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<Strategy> Strategies { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Ward> Wards { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Contact>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<Contract>()
                .Property(e => e.InsuranceName)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Insurance1>()
                .Property(e => e.Price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<Insurance1>()
                .Property(e => e.MaxLimit)
                .HasPrecision(18, 0);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.Price)
                .HasPrecision(18, 0);

            modelBuilder.Entity<OrderDetail>()
                .Property(e => e.AddressContract)
                .IsFixedLength();

            modelBuilder.Entity<PayMent>()
                .Property(e => e.Amount)
                .HasPrecision(18, 0);

            modelBuilder.Entity<PeopleSayU>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<PolicyInsurance>()
                .Property(e => e.Content)
                .IsUnicode(false);

            modelBuilder.Entity<Strategy>()
                .Property(e => e.Content)
                .IsUnicode(false);
        }
    }
}
