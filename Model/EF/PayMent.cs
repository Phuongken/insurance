namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PayMent")]
    public partial class PayMent
    {
        public int ID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public decimal Amount { get; set; }

        public int UserID { get; set; }

        public int ContractID { get; set; }

        public DateTime CreateDate { get; set; }

        public int Status { get; set; }
    }
}
