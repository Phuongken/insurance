namespace Model.EF
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PolicyInsurance")]
    public partial class PolicyInsurance
    {
        public int ID { get; set; }

        public int InsuranceID { get; set; }

        [Required]
        [StringLength(150)]
        public string Title { get; set; }

        [Required]
        [StringLength(150)]
        public string Name { get; set; }

        [Column(TypeName = "text")]
        [Required]
        public string Content { get; set; }

        public int Status { get; set; }

        public DateTime CreateDate { get; set; }

        public DateTime UpdateAt { get; set; }

        public DateTime ModifiledDate { get; set; }

        public DateTime ModifiledAt { get; set; }

        [Required]
        [StringLength(250)]
        public string Image { get; set; }
    }
}
