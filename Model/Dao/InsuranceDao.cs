﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class InsuranceDao
    {
        Model1 db = null;
        public InsuranceDao()
        {
            db = new Model1();
        }
        public bool Insert(Insurance1 insurance)
        {
            try
            {
                db.Insurance1.Add(insurance);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<Insurance1> Index()
        {
            return db.Insurance1.ToList();
        }
        public Insurance1 find(int? id)
        {
            return db.Insurance1.Find(id);
        }
        public bool Update(Insurance1 newInsurance1)
        {
            try
            {
                var insurance = db.Insurance1.Find(newInsurance1.ID);
                insurance.Name = newInsurance1.Name;
                insurance.Avatar = newInsurance1.Avatar;
                insurance.Price = newInsurance1.Price;
                insurance.MaxLimit = newInsurance1.MaxLimit;
                insurance.ModifiledDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var insurance = db.Insurance1.Find(id);
                
                    db.Insurance1.Remove(insurance);
                    db.SaveChanges();
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
