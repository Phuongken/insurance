﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model.EF;

namespace Model.Dao
{
    public class PolicyInsuranceDao
    {
        Model1 db = null;
        public PolicyInsuranceDao()
        {
            db = new Model1();
        }
        public bool Insert(PolicyInsurance policyInsurance, int id)
        {
            try
            {
                
                if (id != policyInsurance.InsuranceID)
                {
                    return false;
                }
                else
                {
                    policyInsurance.InsuranceID = id;
                    db.PolicyInsurances.Add(policyInsurance);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public List<PolicyInsurance> Index()
        {
            return db.PolicyInsurances.ToList();
        }
        public PolicyInsurance find(int? id)
        {
            return db.PolicyInsurances.Find(id);
        }
        public bool Update(PolicyInsurance newPolicyInsurance)
        {
            try
            {
                var insurance = db.PolicyInsurances.Find(newPolicyInsurance.ID);
                insurance.Title = newPolicyInsurance.Title;
                insurance.Name = newPolicyInsurance.Name;
                insurance.Content = newPolicyInsurance.Content;
                insurance.Image = newPolicyInsurance.Image;
                insurance.ModifiledDate = DateTime.Now;
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        public bool Delete(int? id)
        {
            try
            {
                var policyInsurance = db.PolicyInsurances.Find(id);
                db.PolicyInsurances.Remove(policyInsurance);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
